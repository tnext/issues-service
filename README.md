# Issues service

## Prerequisites: 
- node 12.*(used 12.18.3)
- postgress 10.15

## How to run project
- copy values from .sample.env into .env `cp .env.sample .env`
- please paste relevant values for postgress database
- please create 2 SQS FIFO ques and paste values in .env
- install dependencies `npm install`
- create db: `npm run create:db`
- run migrations: `npm run migrate:up`
- run seeders: `npm run seed`
- start service by `npm run start:dev`

## Postman:
- will provide postman collection with `POST_issue` and `mark_resolved` requests
- will provide postman environment LOCAL_ISSUES for running postman collection
- `POST_issue` - creates new issue
- `mark_resolved` - will send pre-request script to create new issue, and will mark it complete

## Testing:
- there are two types of tests functional that work with database, and unit that use only mocks.
- run `npm test` to run unit tests, absolutely safe, don't do any work with DB or any other service.
- run `test:f` to run functional tests. You can run it in your current local database(at the moment it is harmless) or setup test database

## Notes
> Project is functional but is still required updates and proper testing
> *logger*
>> TODO add morgan for logging HTTP

> Should have used interface like IUser outside of services folder, not UserDBModel in all places

