import { NextFunction, Response } from "express-async-router";
import { Transaction } from "sequelize";
import { IssueDBModel, SupportAgentDBModel, UserDBModel } from "../../database";
import { Transactional } from "../../database/transactions";
import { IRequestExtended } from "../../models";
import { IIssueService, issueService, ISupportAgentService, supportAgentService } from "../../services";
import { BaseController } from "../_base";
import { boundClass } from 'autobind-decorator';
import {
  IIssueProvider,
  IIssueConsumer,
  issueProvider,
  issueConsumer,
  ISupportAgentConsumer,
  supportAgentConsumer,
  ISupportAgentProvider,
  supportAgentProvider
} from "../../queues";
import { ApiError } from "../../errors";
import { Logger } from "../../helpers";
import { IssueStatusEnum } from "../../constants";

interface IIssueControllerServices {
  issueService: IIssueService;
  issueProvider: IIssueProvider;
  issueConsumer: IIssueConsumer;
  supportAgentService: ISupportAgentService;
  supportAgentConsumer: ISupportAgentConsumer;
  supportAgentProvider: ISupportAgentProvider;
}

const log = new Logger('CNTR:ISSUE')

@boundClass
export class IssueController extends BaseController<IIssueControllerServices> {
  @Transactional
  async postIssue(req: IRequestExtended, res: Response, next: NextFunction, transaction?: Transaction) {
    const userId = (req.user as UserDBModel).id;
    const { description } = req.body;

    const availableAgent = await this.services.supportAgentConsumer.getOneAgent();

    let issue: IssueDBModel;

    if (availableAgent) {
      // there is agent create issue with agent
      issue = await this.services.issueService.createIssue({
        description,
        userId,
        supportAgentId: availableAgent ? (availableAgent.body as SupportAgentDBModel).id : null,
        status: IssueStatusEnum.ASSIGNED
      }, transaction);
      // remove agent from que
      await this.services.supportAgentConsumer.deleteFreeAgent(availableAgent.receiptHandle);
    } else {
      // no agents free create issue without agent
      issue = await this.services.issueService.createIssue({
        description,
        userId
      }, transaction);
      // add issue to que
      await this.services.issueProvider.addMessage(JSON.stringify(issue));
    }

    log.silly(`[postIssue] issue created: ${issue.id} status: ${issue.status}`);
    return { data: { issue } };
  }

  @Transactional
  async markComplete(req: IRequestExtended, res: Response, next: NextFunction, transaction?: Transaction) {
    const userId = (req.user as UserDBModel).id;
    const issue = req.state?.issue as IssueDBModel;
    const issueId = issue.id;
    let agent = req.state?.support_agent as SupportAgentDBModel;

    // check if issue is assigned to this agent
    if (issue.supportAgentId !== agent.id) {
      throw ApiError.forbidden('You are not assigned to this issue');
    }

    log.silly(`[markComplete] agentUserId: ${userId} issueId: ${issueId}`);

    // mark update support agent
    agent = await this.services.supportAgentService.updateOneById(agent.id,
      {
        lastClosedIssueDate: new Date()
      },
      transaction
    );
    // resolve issue
    const resolvedIssue = await this.services.issueService.markResolved(issueId, transaction);
    let newIssue: IssueDBModel | null = null;

    // pull for new issues
    let issueMessage = await this.services.issueConsumer.getOneMessage();

    // issue found assign to agent
    if (issueMessage) {
      newIssue = await this.services.issueService.assignIssue(agent.id, (issueMessage.body as IssueDBModel).id, transaction);
      // remove issue from que
      this.services.issueConsumer.delete(issueMessage.receiptHandle);
    } else {
      // issue not found add him to que...
      await this.services.supportAgentProvider.addFreeAgent(JSON.stringify(agent));
    }


    log.silly(`[markComplete] issue resolved: ${resolvedIssue.id} new issue assigned: ${newIssue ? newIssue.id : null}`);

    return { data: { issue: resolvedIssue, supportAgent: agent, newIssue } };
  }
}

export const issueController = new IssueController({
  issueService,
  issueProvider,
  issueConsumer,
  supportAgentService,
  supportAgentConsumer,
  supportAgentProvider
});