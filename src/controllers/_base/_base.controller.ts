export abstract class BaseController<S> {
  services: S;
  constructor(services: S) {
    this.services = services;
  }
}