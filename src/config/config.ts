import convict from "convict";
import dotenv from 'dotenv';
import fs from 'fs';
import path from 'path';
import { CronPeriodsEnum } from "../cron";
import { Logger } from "../helpers";

dotenv.config();
const log = new Logger('Config');

// Define a schema
const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['local', 'development', 'test', 'production'],
    default: 'local',
    env: 'NODE_ENV'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 3005,
    env: 'PORT',
    arg: 'port'
  },
  db: {
    username: {
      format: String,
      default: 'postgres',
      env: 'DB_USERNAME'
    },
    password: {
      format: String,
      default: 'root',
      env: 'DB_PASSWORD',
    },
    database: {
      format: String,
      default: 'tn_issues',
      env: 'DB_NAME'
    },
    port: {
      format: String,
      default: '5432',
      env: 'DB_PORT'
    },
    host: {
      format: String,
      default: 'localhost',
      env: 'DB_HOST'
    }
  },
  aws: {
    region: {
      format: String,
      default: 'eu-central-1',
      env: 'AWS_REGION'
    },
    accessKeyId: {
      format: String,
      default: 'DEFAULT_ACCESS_KEY',
      env: 'AWS_ACCESS_KEY'
    },
    secretAccessKey: {
      format: String,
      default: 'default_secret_key',
      env: 'AWS_SECRET_KEY'
    }
  },
  queue: {
    issue: {
      url: {
        format: String,
        default: 'ISSUE_QUE_URL',
        env: 'ISSUE_QUE_URL'
      }
    },
    supportAgent: {
      url: {
        format: String,
        default: 'SUPPORT_AGENT_QUE_URL',
        env: 'SUPPORT_AGENT_QUE_URL'
      }
    }
  },
  cron: {
    [CronPeriodsEnum.Smallest]: {
      format: String,
      default: '*/5 * * * *',
      env: 'CRON_PERIOD_SMALLEST'
    }
  }
});

// Load environment dependent configuration
const env = config.get('env');
const configFolderPath = path.join(__dirname, '../../env_configs/', `${env}.json`);
try {
  const configExists = fs.existsSync(configFolderPath);
  if (configExists) {
    config.loadFile(configFolderPath);
  }
} catch (error) {
  log.error(error);
}

// Perform validation
config.validate({ allowed: 'strict' });

export { config };