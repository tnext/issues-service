export class ApiError extends Error {
  statusCode?: number;

  constructor(statusCode: number, message: string) {
    super(message);
    this.statusCode = statusCode;
  }

  static unauthorized(message = 'Unauthorized') {
    return new ApiError(401, message);
  }

  static badRequest(message = 'Bad request') {
    return new ApiError(400, message);
  }

  static forbidden(message = 'Forbidden') {
    return new ApiError(403, message);
  }

  static notFound(message = 'Not found') {
    return new ApiError(404, message);
  }

  static internal(message = 'Server error') {
    return new ApiError(500, message);
  }
}