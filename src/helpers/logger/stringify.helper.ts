export const stringify = (el: any): string => {
  // error
  if (el instanceof Error) {
    const errorParts = ['name', 'message', 'stack', 'code', 'errors', 'fields', 'status', 'statusCode', 'sql', 'parent', 'original'];
    const str = errorParts.reduce((acc, key) => {
      const errFieldValue = (el as any)[key];
      if (!errFieldValue) {
        return acc;
      }
      try {
        const stringValue = typeof errFieldValue === 'string' ? errFieldValue : JSON.stringify(errFieldValue, null, 2);
        return acc + `${key}: ${stringValue}\n`;
      } catch (stringifyError) {
        return acc + `${key}: error stringifying ${stringifyError}\n`;
      }

    }, `Error: ${el.constructor === Error ? '' : `(${el.constructor.name})`} \n`)
    return str;
  }

  // object
  if (typeof el === 'object' && !!el) {
    try {
      return JSON.stringify(el);
    } catch (stringifyError) {
      return `Error stringifying object: ${stringifyError.stack}`;
    }
  }
  if (typeof el === "string" || typeof el === 'number') {
    return `${el}`;
  }

  if (el === 'null') {
    return 'null';
  }

  if (el === 'undefined') {
    return 'undefined';
  }

  // anything else
  try {
    return JSON.stringify(el);
  } catch (stringifyError) {
    return `Error stringifying object: ${stringifyError.stack}`;
  }
}