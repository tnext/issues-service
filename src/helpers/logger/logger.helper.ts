import winston from "winston";
import path from 'path';
import { stringify } from "./stringify.helper";

const { combine, timestamp, printf, label } = winston.format;

type LevelNPM = 'error' | 'warn' | 'info' | 'http' | 'verbose' | 'debug' | 'silly';

export class Logger {
  private prefix: string;
  private logger: winston.Logger;

  constructor(prefix: string) {
    this.prefix = prefix;
    this.logger = winston.createLogger({
      exitOnError: false,
      level: "silly",
      format: combine(
        timestamp(),
        label({ label: this.prefix }),
        printf(({ timestamp, message, label, level }) => `${timestamp} [${level.toUpperCase()}] [${label}]: ${message}`)),
      defaultMeta: { service: 'user-service' },
      transports: [
        new winston.transports.File({ filename: path.join(__dirname, '../../../logs/' + 'error.log'), level: 'error' }),
        new winston.transports.File({ filename: path.join(__dirname, '../../../logs/' + 'combined.log') }),
        new winston.transports.Console()
      ],
    })
  }

  silly(...args: any[]) {
    this.log('silly', args)
  }
  debug(...args: any[]) {
    this.log('debug', args)
  }
  verbose(...args: any[]) {
    this.log('verbose', args)
  }
  http(...args: any[]) {
    this.log('http', args)
  }
  info(...args: any[]) {
    this.log('info', args)
  }
  warn(...args: any[]) {
    this.log('warn', args)
  }
  error(...args: any[]) {
    this.log('error', args)
  }

  private log(level: LevelNPM, args: any[]) {
    this.logger.log(level, args.map(arg => stringify(arg)).join(' '));
  }
}