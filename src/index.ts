import { config } from "./config";
import { Cron } from "./cron/init.cron";
import { Logger } from "./helpers";
import { issueConsumer, supportAgentConsumer } from "./queues";
import { Server } from "./server";
import { issueService } from "./services";

const server = new Server(config.get('port'));

const log = new Logger('Server');

server.start(() =>
  log.info(`server listening on port ${config.get('port')}`)
);

const cron = new Cron({
  issueService,
  issueConsumer,
  supportAgentConsumer
});
cron.start();

process.on('SIGTERM', () => {
  log.error('Received SIGTERM - terminating:');
  server.terminate();
  cron.terminate();
});

process.on('uncaughtException', (error: Error) => {
  log.error('Uncaught Exception:', error);
});
process.on('unhandledRejection', (error: any, promise: Promise<any>) => {
  log.error('Unhandled promise rejection.', 'Error: ', error, 'Promise: ', promise);
});
