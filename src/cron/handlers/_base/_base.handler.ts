import { CronPeriodsEnum } from "../../periods.enum";

interface IBaseHanlder { };

export abstract class BaseHandler<S> implements IBaseHanlder {
  static getType: () => CronPeriodsEnum;
  readonly services: S;
  constructor(services: S) {
    this.services = services;
  }
}