import { Logger } from "../../../helpers";
import { IIssueConsumer, ISupportAgentConsumer, supportAgentConsumer } from "../../../queues";
import { IIssueService } from "../../../services";
import { CronPeriodsEnum } from "../../periods.enum";
import { IHandler } from "../interfaces";
import { BaseHandler } from "../_base";

interface IMatchIssueToAgentHandler {
  issueService: IIssueService;
  issueConsumer: IIssueConsumer;
  supportAgentConsumer: ISupportAgentConsumer;
}

const log = new Logger('IssueToAgent:CronHandler');

export class MatchIssueToAgentHandler extends BaseHandler<IMatchIssueToAgentHandler> implements IHandler {
  static getType() {
    return CronPeriodsEnum.Smallest;
  }

  async exec() {
    // find issues in que
    const issues = await this.services.issueConsumer.getMessages(10);

    if (issues.length) {

      log.verbose(`issues found : ${issues.length}`);

      // find available agents
      const agents = await this.services.supportAgentConsumer.getAgents(10);

      if (agents.length) {

        log.verbose(`agents found : ${agents.length}`);

        for (let i = 0; i < Math.min(agents.length, issues.length); i++) {
          const issue = issues[i];
          const agent = agents[i];
          // TODO assign issue to agent

          // TODO remove agent from free agents

          // TODO remove issue from que
        }
      }

      return;
    }


    log.verbose(`issue not found`);
  }
}