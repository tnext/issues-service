export interface IHandler {
  exec(): Promise<void>;
}