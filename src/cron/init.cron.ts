import cron from "node-cron";
import { CronPeriodsEnum } from "./periods.enum";
import * as handlers from "./handlers";
import { IIssueService } from "../services";
import { IIssueConsumer, ISupportAgentConsumer } from "../queues";
import { config } from "../config";
import { Logger } from "../helpers";

const log = new Logger('CRON');

export interface ICronServices {
  issueService: IIssueService;
  issueConsumer: IIssueConsumer;
  supportAgentConsumer: ISupportAgentConsumer;
}

export class Cron {
  private scheduledTasks: Map<CronPeriodsEnum, cron.ScheduledTask>;
  private services: ICronServices;

  constructor(services: ICronServices) {
    this.scheduledTasks = new Map();
    this.services = services;
  }

  start() {

    log.verbose(`Start scheduling cron works...`);

    // get all handlers
    const handlersArray = Object.values(handlers);

    // iterate over every period and schedule every job for that period
    for (const period of Object.values(CronPeriodsEnum)) {

      // schedule for this period function that will execute all handlers
      const scheduledTask = cron.schedule(config.get('cron')[period], async () => {

        log.verbose(`executing ${period} period`);

        const instances: handlers.IHandler[] = handlersArray
          // filter only handlers for this period
          .filter(handlerClass => {
            if (typeof handlerClass?.getType !== 'function') {
              return false;
            } else {
              return handlerClass.getType() === period
            }
          })
          // construct instance from classes
          .map(handlerClass => Reflect.construct(handlerClass, [this.services]));


        // run handlers for this period
        for (const handler of instances) {
          await handler.exec();
        }


        log.verbose(`finish iteration of ${period} period...`);
      })

      // add this to Map, so we can terminate them
      this.scheduledTasks.set(period, scheduledTask);
    }


    log.verbose(`Finish scheduling cron works...`);
  }

  terminate() {
    for (const [period, scheduledTask] of this.scheduledTasks) {

      log.verbose(`Destroying ${period} period`);
      scheduledTask.destroy();
    }
  }
}