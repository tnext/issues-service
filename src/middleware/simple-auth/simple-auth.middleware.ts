import { NextFunction, Response } from "express-async-router";
import { RequestHeadersEnum } from "../../constants";
import { ApiError } from "../../errors";
import { IRequestExtended } from "../../models";

export const simpleAuthMiddleware = (req: IRequestExtended, res: Response, next: NextFunction) => {
  // from Authorization header get user id
  const userId = req.get(RequestHeadersEnum.AUTHORIZATION);

  if (!userId) {
    return next(ApiError.unauthorized())
  }

  // here should get user from user service, for simplicity will be only id in user
  req.user = { id: +userId };
  next();
}