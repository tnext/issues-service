import { NextFunction, Response } from "express-async-router";
import { IReqState, IRequestExtended } from "../../models";

/**
 * Middleware to make sure there is state object in request
 * to avoid Uncaught TypeError: Cannot read property of undefined error
 * @param req request
 * @param res response
 * @param next next
 */
export const setStateMiddleware = (req: IRequestExtended, res: Response, next: NextFunction) => {
  req.state = req.state || {} as IReqState;
  next();
}