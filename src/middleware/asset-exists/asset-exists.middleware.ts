import { NextFunction, Response } from "express-async-router";
import { IRequestExtended } from "../../models";
import { Logger } from "../../helpers";
import { ApiError } from "../../errors";

const log = new Logger('MDWR:AssetExists');

/**
 * Finds asset and adds to state
 * @param model db model to find asset
 * @param key if string provided, in req.params the name is the same as primary key in Model, like 'id'
 * if array is provided, first is key in params, second is key in model, like ['issue_id', 'id']
 * @param softCheck throw error if asset not found
 * @example assetExistsMiddleware(IssueDBModel, ['issue_id', 'id']);
 * @example assetExistsMiddleware(IssueDBModel, 'id'); // so req.params.id
 */
export const assetExistsMiddleware = (model: any, key: string | [string, string], softCheck = false) => async (req: IRequestExtended, res: Response, next: NextFunction) => {
  const [keyInParams, keyInModel] = Array.isArray(key) ? key : [key, key];
  const asset = await model.findOne({
    where: {
      [keyInModel]: req.params[keyInParams],
    },
    raw: true
  });

  if (!softCheck && !asset) {
    return next(ApiError.notFound(`${model.name} not found!`));
  }

  if (asset) {
    (req.state as any)[model.name] = asset;
  }

  next();
}