import { NextFunction, Response } from "express-async-router";
import { IRequestExtended } from "../../models";
import { ApiError } from "../../errors";
import { DatabaseModelNames } from "../../database";

/**
 * Compare status of entity in state with requiredStatus
 * @param modelName name of model in state
 * @param requiredStatus what status should be
 * @param statusField how statusField is called in Model
 * @example checkStatusMiddleware(DatabaseModelNames.issue, '0');
 */
export const checkStatusMiddleware = (modelName: DatabaseModelNames, requiredStatus: string | number, statusField = 'status') =>
  async (req: IRequestExtended, res: Response, next: NextFunction) => {
    const entity = req?.state?.[modelName]
    if (!entity) {
      return next(ApiError.internal(`No ${modelName} in state`));
    }

    const status = (entity as any)[statusField];

    if (!status) {
      return next(ApiError.internal(`No ${status} in ${modelName} entity`));
    }

    // != not !==, to allow comparison between number and string
    if (status != requiredStatus) {
      return next(ApiError.badRequest(`${modelName} has wrong status ${status}, required ${requiredStatus}`));
    }

    next();
  }