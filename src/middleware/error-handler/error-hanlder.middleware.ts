import { NextFunction, Request, Response } from "express-async-router";
import * as Joi from "joi";
import { Logger } from "../../helpers";

const log = new Logger('MDWR:ErrorHandler');

/**
 * Simplified error handler middleware
 * @param showInternalError show to client internal of errors(should be false in production)
 */
export const errorHandlerMiddleware = (showInternalError = false) => (err: any, req: Request, res: Response, next: NextFunction) => {

  log.error(err);

  let statusCode = err.statusCode || 500;
  let error = err;

  // Joi Validation error
  // TODO create custom ValidationError not to get locked to Joi
  if (error instanceof Joi.ValidationError) {
    statusCode = 400;
    error.message = error?.details[0]?.message;
  }

  res.status(statusCode);

  if (showInternalError || statusCode < 500) {
    // TODO format err
    res.send({ body: { error: err.toString() } })
  }

  if (!showInternalError) {
    res.send({ body: { error: "Something went wrong" } })
  }
}