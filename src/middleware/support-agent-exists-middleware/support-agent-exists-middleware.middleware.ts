import { NextFunction, Response } from "express-async-router";
import { ApiError } from "../../errors";
import { IRequestExtended } from "../../models";
import { supportAgentService } from "../../services";
import { IReqState } from "../../models";

export const supportAgentExistsMiddleware = async (req: IRequestExtended, res: Response, next: NextFunction) => {
  const userId = req.user?.id;
  if (!userId) {
    return next(ApiError.unauthorized('No user for support agent'));
  }

  const supportAgent = await supportAgentService.getOneByParams({
    userId
  });

  if (!supportAgent) {
    return next(ApiError.badRequest(`No support agent with userId: ${userId}`));
  }

  (req.state as IReqState).support_agent = supportAgent;
  next();
}