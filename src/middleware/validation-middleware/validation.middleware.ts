import { NextFunction, Response } from "express-async-router";
import { IRequestExtended } from "../../models";
import * as Joi from "joi";
import { Logger } from "../../helpers";

const log = new Logger('MDWR:Validation');

export const validationMiddleware = (reqSchema: {
  body?: Joi.Schema;
  params?: Joi.Schema;
  query?: Joi.Schema;
}) => (req: IRequestExtended, res: Response, next: NextFunction) => {
  const schema = Joi.object({
    body: Joi.alternatives().try(Joi.object(), Joi.array()),
    params: Joi.object(),
    query: Joi.object()
  }).keys(reqSchema);

  try {
    const { error } = schema.validate({
      body: req.body,
      params: req.params,
      query: req.query
    });
    if (error) {
      log.verbose(error?.details[0]?.message);
      next(error);
    }
  } catch (error) {
    // TODO log.error here
    log.error(error);
    next(error);
  }

  next();
}