import express, { Express } from 'express'
import { config } from './config';
import { errorHandlerMiddleware, setStateMiddleware } from './middleware';
import { router } from './router';

export class Server {
  private _server: Express;
  private _port: number;

  constructor(port: number) {
    this._server = express();
    this._port = port;
    this._server.use(setStateMiddleware);
    this._server.use(express.json());
    this._server.use(router);
    this._server.use(errorHandlerMiddleware(['local', 'development'].includes(config.get('env'))));
  }

  start(clbck?: () => void) {
    this._server.listen(this._port, clbck);
  }

  terminate() {
    // TODO close server
  }
}