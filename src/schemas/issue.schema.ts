import * as Joi from "joi";

export const IssueSchema = Joi.object().keys({
  description: Joi.string().min(15).max(1000).required()
})