import AWS from "aws-sdk";
import { config } from "../../config";

const { region, accessKeyId, secretAccessKey } = config.get('aws');

AWS.config.update({ region, accessKeyId, secretAccessKey });

export { AWS as AWSWithConfig };