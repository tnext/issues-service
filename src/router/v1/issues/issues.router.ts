import { AsyncRouter } from "express-async-router";
import { issueController } from "../../../controllers";
import { IssueSchema } from "../../../schemas";
import { assetExistsMiddleware, checkStatusMiddleware, simpleAuthMiddleware, supportAgentExistsMiddleware, validationMiddleware } from "../../../middleware";
import { DatabaseModelNames, IssueDBModel } from "../../../database";
import { IssueStatusEnum } from "../../../constants";

const router = AsyncRouter();

router.use('/', simpleAuthMiddleware);

router.post('/', validationMiddleware({ body: IssueSchema }), issueController.postIssue);

router.patch('/:issue_id/complete',
  supportAgentExistsMiddleware,
  assetExistsMiddleware(IssueDBModel as any, ['issue_id', 'id']),
  checkStatusMiddleware(DatabaseModelNames.issue, IssueStatusEnum.ASSIGNED),
  issueController.markComplete
);

export { router as issuesRouter }; 