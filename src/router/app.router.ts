import { AsyncRouter } from "express-async-router";
import { issuesRouter } from "./v1";

const appRouter = AsyncRouter();

appRouter.use('/issues', issuesRouter);

export { appRouter }