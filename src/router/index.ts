import { AsyncRouter } from "express-async-router";
import { appRouter as v1ApiRouter } from "./app.router";

const router = AsyncRouter();

router.use('/api/v1', v1ApiRouter);

router.get('/ping', async (req, res) => {
  res.send({ data: 'pong' });
})
// status
router.get('/status', (req, res) => {
  const { rss, heapTotal, heapUsed } = process.memoryUsage();
  const bytesToMb = (bytes: number) => Math.round((bytes / 1024 / 1024) * 100) / 100;
  res.send({
    data: {
      timestamp: new Date().toUTCString(),
      status: "available",
      rss: bytesToMb(rss),
      heapTotal: bytesToMb(heapTotal),
      heapUsed: bytesToMb(heapUsed),
    }
  })
})

export { router }