import { Logger } from "../../helpers";
import { QueueBase, IMessageReceived } from "../_base";

const log = new Logger('SupportAgentConsumer');

export interface ISupportAgentConsumer {
  getAgents(numberOfAgents: number, attributes?: string[]): Promise<IMessageReceived[]>;
  getOneAgent(attributes?: string[]): Promise<IMessageReceived | null>;
  deleteFreeAgent(receiptHandle: string): Promise<void>;
}

export class SupportAgentConsumer extends QueueBase implements ISupportAgentConsumer {
  async getAgents(numberOfAgents = 10, attributes?: string[]): Promise<IMessageReceived[]> {
    const messages = await this.receiveMessages({ attributes, numberOfMessages: numberOfAgents });

    log.verbose(`[getAgents] response from service`, messages);
    return messages;
  }

  async getOneAgent(attributes?: string[]): Promise<IMessageReceived | null> {
    const messages = await this.receiveMessages({ attributes, numberOfMessages: 1 });

    log.verbose(`[getOneAgent] response from service`, messages);
    return messages[0];
  }

  async deleteFreeAgent(receiptHandle: string): Promise<void> {
    await this.deleteMessage(receiptHandle);

    log.verbose(`[deleteFreeAgent] message, receiptHandle: ${receiptHandle}`);
  }
}