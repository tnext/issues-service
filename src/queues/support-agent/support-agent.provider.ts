import { Logger } from "../../helpers";
import { QueueBase, ISendMessageResult } from "../_base";

const log = new Logger('SupportAgentProvider');

export interface ISupportAgentProvider {
  addFreeAgent(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult>;
}

export class SupportAgentProvider extends QueueBase implements ISupportAgentProvider {
  async addFreeAgent(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult> {
    const result = await this.sendMessage(body, { attributes });
    log.verbose(`[addFreeAgent] response from service`, result);
    return result;
  }
} 