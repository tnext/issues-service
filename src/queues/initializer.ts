import { config } from "../config";
import { AWSWithConfig } from "../intergations";
import { IssueProvider, IssueConsumer } from "./issue";
import { SupportAgentConsumer, SupportAgentProvider } from "./support-agent";

const sqs = new AWSWithConfig.SQS({ apiVersion: '2012-11-05' });
const { url: issueUrl } = config.get('queue').issue;
const { url: supportAgentUrl } = config.get('queue').supportAgent;

export const issueConsumer = new IssueConsumer(sqs, issueUrl);
export const issueProvider = new IssueProvider(sqs, issueUrl);

export const supportAgentConsumer = new SupportAgentConsumer(sqs, supportAgentUrl);
export const supportAgentProvider = new SupportAgentProvider(sqs, supportAgentUrl);
