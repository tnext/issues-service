import { SQS } from "aws-sdk"
import { promisify } from "util";
import { ISendMessageResult, IMessageReceived } from "./interfaces";
import * as uuid from "uuid";
import { Logger } from "../../helpers";

const log = new Logger('QueueBase');

export interface IQueueBase { };

/**
 * Wrapper around SQS
 * requires SQS apiVersion: '2012-11-05'
*/
export abstract class QueueBase implements IQueueBase {
  private _queue: SQS;
  private _queueUrl: string;
  constructor(sqs: SQS, queueUrl: string) {
    this._queue = sqs;
    this._queueUrl = queueUrl;

    this._queue.sendMessage = this._queue.sendMessage.bind(this._queue);
    this._queue.receiveMessage = this._queue.receiveMessage.bind(this._queue);
    this._queue.deleteMessage = this._queue.deleteMessage.bind(this._queue);
  }

  protected async sendMessage(body: string, params: { attributes?: Map<string, string> }): Promise<ISendMessageResult> {
    const { attributes = [] } = params;
    const messageAttributes = !attributes ? {} : Array.from(attributes).reduce<SQS.MessageBodyAttributeMap>((acc, [key, value]) => {
      acc[key] = {
        DataType: "String",
        StringValue: value
      }
      return acc;
    }, {})
    const { MessageId } = await promisify<SQS.SendMessageRequest, SQS.SendMessageResult>(this._queue.sendMessage)({
      MessageBody: body,
      QueueUrl: this._queueUrl,
      MessageDeduplicationId: uuid.v4(),
      MessageAttributes: messageAttributes,
      MessageGroupId: uuid.v4() // use session data if needed https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/using-messagegroupid-property.html
    });
    return { messageId: MessageId as string };
  }

  protected async receiveMessages(params: { attributes?: string[], numberOfMessages?: number, visibilityTimeout?: number }): Promise<IMessageReceived[]> {
    const {
      attributes = [],
      numberOfMessages = 1,
      visibilityTimeout = 5
    } = params;
    const { Messages } = await promisify<SQS.ReceiveMessageRequest, SQS.ReceiveMessageResult>(this._queue.receiveMessage)({
      AttributeNames: [
        "SentTimestamp"
      ],
      MaxNumberOfMessages: numberOfMessages,
      MessageAttributeNames: attributes,
      QueueUrl: this._queueUrl,
      VisibilityTimeout: visibilityTimeout,
      WaitTimeSeconds: 0
    });

    let messages: IMessageReceived[];
    try {
      messages = Messages ? Messages.map(message =>
      ({
        body: JSON.parse(message?.Body as string),
        timestamp: message?.Attributes?.SentTimestamp as string,
        attributes: message?.MessageAttributes as unknown as Record<string, string>,
        receiptHandle: message?.ReceiptHandle as string
      })) : [];
      return messages
    } catch (error) {

      log.error(`Error parsing messages JSON`, Messages?.map(m => m.Body).join(';\n'));
      return [];
    }
  }

  protected deleteMessage(receiptHandle: string): Promise<{}> {
    return promisify<SQS.DeleteMessageRequest, {}>(this._queue.deleteMessage)({
      ReceiptHandle: receiptHandle,
      QueueUrl: this._queueUrl
    });
  }
}