export interface ISendMessageResult {
  messageId: string
}

export interface IMessageReceived {
  body: Object;
  timestamp: string;
  attributes?: Record<string, string>;
  receiptHandle: string;
}