import { Logger } from "../../helpers";
import { QueueBase, ISendMessageResult } from "../_base";

const log = new Logger('IssueProvider');

export interface IIssueProvider {
  addMessage(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult>;
}

export class IssueProvider extends QueueBase implements IIssueProvider {
  async addMessage(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult> {
    const result = await this.sendMessage(body, { attributes });

    log.verbose(`[addMessage] response from service`, result);
    return result;
  }
}