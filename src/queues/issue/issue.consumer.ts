import { Logger } from "../../helpers";
import { QueueBase, IMessageReceived } from "../_base";

const prefix = '[IssueConsumer]';
const log = new Logger('IssueConsumer');

export interface IIssueConsumer {
  getMessages(numberOfMessages: number, attributes?: string[]): Promise<IMessageReceived[]>;
  getOneMessage(attributes?: string[]): Promise<IMessageReceived | null>;
  delete(receiptHandle: string): Promise<void>;
}

export class IssueConsumer extends QueueBase implements IIssueConsumer {
  async getMessages(numberOfMessages = 10, attributes?: string[]): Promise<IMessageReceived[]> {
    const messages = await this.receiveMessages({ attributes, numberOfMessages });

    log.verbose(`[getMessages] response from service`, messages);
    return messages;
  }

  async getOneMessage(attributes?: string[]): Promise<IMessageReceived | null> {
    const messages = await this.receiveMessages({ attributes, numberOfMessages: 1 });

    log.verbose(`[getOneMessage] response from service`, messages);
    return messages[0];
  }

  async delete(receiptHandle: string): Promise<void> {
    await this.deleteMessage(receiptHandle);

    log.verbose(`[delete] message, receiptHandle: ${receiptHandle}`);
  }
}