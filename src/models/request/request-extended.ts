import { Request } from "express-async-router";
import { UserDBModel, IDatabaseModels } from "../../database";

export interface IReqState extends IDatabaseModels { }

export interface IRequestExtended extends Request {
  user?: Partial<UserDBModel>;
  state?: IReqState;
}