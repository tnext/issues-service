import { Transaction } from "sequelize/types";
import { SupportAgentDBModel } from "../../database";

export interface ISupportAgentService {
  getOneByParams(params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel>;
  updateOneById(agentId: number, params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel>;
}

class SupportAgentService implements ISupportAgentService {
  getOneByParams(params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel> {
    return SupportAgentDBModel.findOne({ where: params, transaction, raw: true }) as Promise<SupportAgentDBModel>;
  }
  async updateOneById(agentId: number, params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel> {
    await SupportAgentDBModel.update(
      params,
      {
        where: { id: agentId },
        transaction
      });
    const issue = await SupportAgentDBModel.findByPk(agentId, { transaction, raw: true });

    return issue as SupportAgentDBModel;
  }
}

export const supportAgentService = new SupportAgentService();