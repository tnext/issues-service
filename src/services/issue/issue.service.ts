import { Transaction } from "sequelize";
import { IssueStatusEnum } from "../../constants";
import { IssueDBModel } from "../../database"

export interface IIssueService {
  createIssue(issue: { description: string, userId: number } & Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel>;
  getOneByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel>;
  deleteIssuesByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<number>;
  assignIssue(agentId: number, issueId: number, transaction?: Transaction): Promise<IssueDBModel>;
  markResolved(issueId: number, transaction?: Transaction): Promise<IssueDBModel>;
}

export class IssueService implements IIssueService {
  getOneByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel> {
    return IssueDBModel.findOne({ where: params, transaction }) as Promise<IssueDBModel>;
  }
  createIssue(issue: { description: string, userId: number } & Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel> {
    return IssueDBModel.create(issue, { transaction, raw: true }) as Promise<IssueDBModel>;
  }
  async deleteIssuesByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<number> {
    return IssueDBModel.destroy({ where: params, transaction });
  }
  async assignIssue(agentId: number, issueId: number, transaction?: Transaction): Promise<IssueDBModel> {
    await IssueDBModel.update(
      {
        status: IssueStatusEnum.ASSIGNED,
        supportAgentId: agentId
      },
      {
        where: { id: issueId },
        transaction
      });
    const issue = await IssueDBModel.findByPk(issueId, { transaction, raw: true });

    return issue as IssueDBModel;
  }

  async markResolved(issueId: number, transaction?: Transaction): Promise<IssueDBModel> {
    await IssueDBModel.update(
      {
        status: IssueStatusEnum.COMPLETE,
        supportAgentId: null
      },
      {
        where: { id: issueId },
        transaction
      });
    const issue = await IssueDBModel.findByPk(issueId, { transaction, raw: true });

    return issue as IssueDBModel;
  }
}

export const issueService = new IssueService();