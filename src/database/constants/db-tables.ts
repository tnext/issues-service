export enum DatabaseTableNames {
  issue = "issues",
  supportAgent = "support_agents",
  user = "users"
}

export enum DatabaseModelNames {
  issue = "issue",
  supportAgent = "support_agent",
  user = "user"
}