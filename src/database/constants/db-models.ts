import { IssueDBModel, SupportAgentDBModel, UserDBModel } from "../models";
import { DatabaseModelNames } from "./db-tables";

export interface IDatabaseModels {
  [DatabaseModelNames.issue]: IssueDBModel;
  [DatabaseModelNames.supportAgent]: SupportAgentDBModel;
  [DatabaseModelNames.user]: UserDBModel;
}

