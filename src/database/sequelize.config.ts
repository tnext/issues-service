import { config } from "../config";
import { Logger } from "../helpers";

const log = new Logger('Sequelize');

const { username, password, database, host, port } = config.get('db');
module.exports = {
  local: {
    username,
    password,
    database,
    host,
    port,
    dialect: "postgres",
    logging: log.silly.bind(log)
  },
  development: {
    username,
    password,
    database,
    host,
    port,
    dialect: "postgres"
  },
  test: {
    username,
    password,
    database,
    host,
    port,
    dialect: "postgres"
  },
  production: {
    username,
    password,
    database,
    host,
    port,
    dialect: "postgres"
  }
}
