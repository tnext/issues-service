import { DataTypes, QueryInterface, QueryOptions } from "sequelize";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";

export const up = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.createTable(DatabaseTableNames.supportAgent,
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        user_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: DatabaseTableNames.user,
            key: 'id'
          }
        },
        last_closed_issue_date: {
          type: DataTypes.DATE,
          allowNull: true
        }
      }, options);
  }
  await migrationWrapper(migration);
}

export const down = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.dropTable(DatabaseTableNames.supportAgent, options)
  }
  await migrationWrapper(migration);
}