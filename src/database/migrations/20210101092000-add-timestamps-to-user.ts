import { DataTypes, QueryInterface, QueryOptions } from "sequelize";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";

export const up = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.addColumn(DatabaseTableNames.user,
      'created_at',
      {
        type: DataTypes.DATE,
        allowNull: false,
      },
      options);
    await queryInterface.addColumn(DatabaseTableNames.user,
      'updated_at',
      {
        type: DataTypes.DATE,
        allowNull: true,
      },
      options);
  }
  await migrationWrapper(migration);
}

export const down = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.removeColumn(DatabaseTableNames.user, 'created_at', options)
    await queryInterface.removeColumn(DatabaseTableNames.user, 'updated_at', options)
  }
  await migrationWrapper(migration);
}