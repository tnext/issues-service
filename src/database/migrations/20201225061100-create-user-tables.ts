import { DataTypes, QueryInterface, QueryOptions } from "sequelize";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";

export const up = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.createTable(DatabaseTableNames.user,
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        name: {
          type: DataTypes.STRING(255),
          allowNull: false
        }
      }, options);
  }
  await migrationWrapper(migration);
}

export const down = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.dropTable(DatabaseTableNames.user, options)
  }
  await migrationWrapper(migration);
}