import { DataTypes, QueryInterface, QueryOptions } from "sequelize";
import { IssueStatusEnum } from "../../constants";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";

export const up = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.createTable(DatabaseTableNames.issue,
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        description: {
          type: DataTypes.STRING(1000),
          allowNull: false
        },
        user_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: DatabaseTableNames.user,
            key: 'id'
          }
        },
        support_agent_id: {
          type: DataTypes.INTEGER,
          allowNull: true,
          onDelete: 'SET NULL',
          references: {
            model: DatabaseTableNames.supportAgent,
            key: 'id'
          }
        },
        status: {
          type: DataTypes.ENUM,
          values: Object.values(IssueStatusEnum).map(value => value.toString()),
          defaultValue: IssueStatusEnum.UNASSIGNED.toString()
        }
      }, options);
  }
  await migrationWrapper(migration);
}

export const down = async (queryInterface: QueryInterface) => {
  const migration = async (options: QueryOptions) => {
    await queryInterface.dropTable(DatabaseTableNames.issue, options)
  }
  await migrationWrapper(migration);
}