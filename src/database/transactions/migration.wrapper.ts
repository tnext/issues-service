import { QueryOptions } from "sequelize";
import { Logger } from "../../helpers";
import { createTransaction } from "./create-transaction.helper";

const log = new Logger('MigrationWrapper');

export const migrationWrapper = async (migration: (options: QueryOptions) => Promise<void>) => {
  const transaction = await createTransaction();

  // to avoid Error stringifying object: TypeError: Converting circular structure to JSON
  // this happens with logger
  (transaction as any).toJSON = () => ({ transaction: 'transaction' });

  try {
    await migration({ transaction, raw: true });
    transaction.commit();
  } catch (error) {
    transaction.rollback();
    log.error(log);
    throw error;
  }
}