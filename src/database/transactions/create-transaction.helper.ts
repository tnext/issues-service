import { db } from "../db.provider"

export const createTransaction = () => db.transaction();