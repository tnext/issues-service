import { createTransaction } from "./create-transaction.helper";

export function Transactional<T, K extends keyof T>(target: T, prop: K, descriptor: PropertyDescriptor) {
  if (!descriptor) {
    throw new Error('Can not get property descriptor');
  }

  const method = descriptor.value;

  if (typeof method !== "function") {
    throw new Error('Only function type supported');
  }

  return {
    ...descriptor,
    async value(...args: any[]) {
      const transaction = await createTransaction();

      // to avoid Error stringifying object: TypeError: Converting circular structure to JSON
      // this happens with logger
      (transaction as any).toJSON = () => ({ transaction: 'transaction' });

      try {
        const result = await method.call(this, ...args, transaction)
        transaction.commit();
        return result;
      } catch (error) {
        transaction.rollback();
        throw error;
      }
    }
  }
}