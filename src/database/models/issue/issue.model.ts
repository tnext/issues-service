import { DataTypes, Model, Optional } from "sequelize";
import { IssueStatusEnum } from "../../../constants";
import { DatabaseModelNames, DatabaseTableNames } from "../../constants";
import { db } from "../../db.provider";
import { Associative } from "../associative.decorator";

interface IIssueModel {
  id: number;
  description: string;
  userId: number;
  supportAgentId: number | null;
  status: IssueStatusEnum;
}

interface IssueCreationAttributes extends Optional<IIssueModel, "id" | "supportAgentId" | "status"> { }

export class IssueDBModel extends Model<IIssueModel, IssueCreationAttributes> implements IIssueModel {
  public id!: number;
  public description!: string;
  public userId!: number;
  public supportAgentId!: number | null;
  public status!: IssueStatusEnum;

  // timestamps
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  @Associative
  static associate({ UserDBModel, SupportAgentDBModel }: any) {
    this.belongsTo(UserDBModel, { foreignKey: "userId" });
    this.belongsTo(SupportAgentDBModel, { foreignKey: "supportAgentId" });
  }
}

IssueDBModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  description: {
    type: DataTypes.STRING(1000),
    allowNull: false
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  supportAgentId: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  status: {
    type: DataTypes.ENUM,
    values: Object.values(IssueStatusEnum).map(value => value.toString()),
    defaultValue: IssueStatusEnum.UNASSIGNED
  }
}, {
  tableName: DatabaseTableNames.issue,
  modelName: DatabaseModelNames.issue,
  sequelize: db,
  timestamps: true,
  underscored: true
})