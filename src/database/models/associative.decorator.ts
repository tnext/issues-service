const models: Record<string, any> = {};

export function Associative(target: any, prop: any, descriptor: PropertyDescriptor) {
  if (!target) {
    throw new Error('Can not get property target!');
  }

  models[target.name] = target;

  return descriptor;
}

export const associateModels = () => {
  Object.values(models).forEach(model => {
    if (model.associate && typeof model.associate === 'function') {
      model.associate(models);
    }
  })
}