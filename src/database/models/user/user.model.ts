import { DataTypes, Model, Optional } from "sequelize";
import { DatabaseModelNames, DatabaseTableNames } from "../../constants";
import { db } from "../../db.provider";
import { Associative } from "../associative.decorator";

interface UserModel {
  id: number;
  name: string;
}

interface UserCreationAttributes extends Optional<UserModel, "id"> { }

export class UserDBModel extends Model<UserModel, UserCreationAttributes> implements UserModel {
  public id!: number;
  public name!: string;

  // timestamps
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  @Associative
  static associate({ IssueDBModel, SupportAgentDBModel }: any) {
    this.hasMany(IssueDBModel, { foreignKey: "userId" });
    this.hasOne(SupportAgentDBModel, { foreignKey: "userId" });
  }
}

UserDBModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(255),
    allowNull: false
  }
}, {
  tableName: DatabaseTableNames.user,
  modelName: DatabaseModelNames.user,
  sequelize: db,
  timestamps: true,
  underscored: true
})