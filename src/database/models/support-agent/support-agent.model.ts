import { DataTypes, Model, Optional } from "sequelize";
import { DatabaseModelNames, DatabaseTableNames } from "../../constants";
import { db } from "../../db.provider";
import { Associative } from "../associative.decorator";

interface SupportAgentModel {
  id: number;
  userId: number;
  lastClosedIssueDate: Date;
}

interface SupportAgentCreationAttributes extends Optional<SupportAgentModel, "id" | "lastClosedIssueDate"> { }

export class SupportAgentDBModel extends Model<SupportAgentModel, SupportAgentCreationAttributes> implements SupportAgentModel {
  public id!: number;
  public userId!: number;
  public lastClosedIssueDate!: Date;

  // timestamps
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  @Associative
  static associate({ UserDBModel, IssueDBModel }: any) {
    this.belongsTo(UserDBModel, { foreignKey: "userId" });
    this.hasOne(IssueDBModel, { foreignKey: "supportAgentId" });
  }
}

SupportAgentDBModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  lastClosedIssueDate: {
    type: DataTypes.DATE,
    allowNull: true
  }
}, {
  tableName: DatabaseTableNames.supportAgent,
  modelName: DatabaseModelNames.supportAgent,
  sequelize: db,
  timestamps: true,
  underscored: true
})