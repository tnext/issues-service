import { associateModels } from "./associative.decorator";

export * from "./issue";
export * from "./support-agent";
export * from "./user";
export * from "./associative.decorator";

// exports before will gather models in models in associative.decorator
// and here we associate them
// call after all exports only!
associateModels();