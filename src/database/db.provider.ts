import { Sequelize } from "sequelize";
import { config } from "../config";
import { Logger } from "../helpers";

const { username, password, database, host, port } = config.get('db');

const log = new Logger('DBProvider');

class DBProvider {
  db: Sequelize;
  constructor() {
    this.db = new Sequelize({
      username,
      password,
      database,
      host,
      port: +port,
      dialect: 'postgres',
      logging: (msg: string) => log.silly(msg)
    })
  }
}

export const db = new DBProvider().db;