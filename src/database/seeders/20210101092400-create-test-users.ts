import { QueryInterface, QueryOptions } from "sequelize";
import { TestEntitiesPrefixesEnum, NUMBER_OF_TEST_USERS, NUMBER_OF_TEST_AGENTS } from "../../constants";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";
import * as uuid from "uuid";

export const up = async (queryInterface: QueryInterface) => {
  const seed = async (options: QueryOptions) => {
    const users: { name: string }[] = new Array(NUMBER_OF_TEST_USERS).fill({}).map((el, index) => ({
      name: `${index === 0 ? 'user' : 'tester'} ${uuid.v1()} ${index === 0 ?
        TestEntitiesPrefixesEnum.user : TestEntitiesPrefixesEnum.supportAgent}`
    }));

    const now = new Date().toISOString().slice(0, 19).replace('T', ' ');

    await queryInterface.sequelize.query(
      `INSERT INTO "${DatabaseTableNames.user}" (name, created_at)
       VALUES ${users.map(({ name }) => `('${name}', '${now}')`).join(',')};`,
      options
    );
  }
  await migrationWrapper(seed);
}

export const down = async (queryInterface: QueryInterface) => {
  const seed = async (options: QueryOptions) => {
    await queryInterface.sequelize.query(
      `DELETE FROM "${DatabaseTableNames.user}"
       WHERE name LIKE '%${TestEntitiesPrefixesEnum.user}' OR name LIKE '%${TestEntitiesPrefixesEnum.supportAgent}'`,
      options
    );
  }
  await migrationWrapper(seed);
}