import { QueryInterface, QueryOptions, QueryTypes } from "sequelize";
import { TestEntitiesPrefixesEnum } from "../../constants";
import { DatabaseTableNames } from "../constants";
import { migrationWrapper } from "../transactions";

export const up = async (queryInterface: QueryInterface) => {
  const seed = async (options: QueryOptions) => {

    const users: { id: number }[] = await queryInterface.sequelize.query(
      `SELECT id FROM "${DatabaseTableNames.user}"
       WHERE name LIKE '%${TestEntitiesPrefixesEnum.supportAgent}'`,
      { ...options, type: QueryTypes.SELECT }
    );

    const now = new Date().toISOString().slice(0, 19).replace('T', ' ');

    await queryInterface.sequelize.query(
      `INSERT INTO "${DatabaseTableNames.supportAgent}" (user_id, created_at)
       VALUES ${users.map(({ id: user_id }) => `('${user_id}', '${now}')`).join(',')};`,
      options
    );
  }
  await migrationWrapper(seed);
}

export const down = async (queryInterface: QueryInterface) => {
  const seed = async (options: QueryOptions) => {
    await queryInterface.sequelize.query(
      // postgres delete join https://www.postgresqltutorial.com/postgresql-delete-join/
      `DELETE FROM "${DatabaseTableNames.supportAgent}" as sa
       USING "${DatabaseTableNames.user}" as u 
       WHERE u.id = sa.user_id      
       AND u.name LIKE '%${TestEntitiesPrefixesEnum.supportAgent}';`,
      options
    );
  }
  await migrationWrapper(seed);
}