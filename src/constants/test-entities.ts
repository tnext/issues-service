export enum TestEntitiesPrefixesEnum {
  user = '__TEST_REGULAR_USER__',
  supportAgent = '__TEST_SUPPORT_USER__'
}

export const NUMBER_OF_TEST_AGENTS = 9;
export const NUMBER_OF_TEST_USERS = NUMBER_OF_TEST_AGENTS + 1;