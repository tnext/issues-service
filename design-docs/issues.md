# How plan to implement:

## Planned DB Structure:
```typescript
interface IssueDBModel {
  id: number;
  description: string;
  userId: number;
  supportAgentId: number | null;
  status: IssueStatusEnum;

  createdAt: Date;
  updatedAT: Date;
}

interface SupportAgent {
  id: number;
  userId: number;
  lastClosedIssueDate: Date;
  
  createdAt: Date;
  updatedAT: Date;
}

enum IssueStatusEnum {
  UNASSIGNED = 0,
  ASSIGNED = 1,
  COMPLETE = 2
}

// should be separate service for users
interface UserDBModel { 
  id: number;
  name: string;

  createdAt: Date;
  updatedAT: Date;
}
```

## Endpoint
> in headers for this task for simplicity test purposes will send userId(should be in token in real life)

### endpoint to post issue POST: `api/v1/issues/`
  - create new issue in database
  - add issue to que of issues 'issueQue'
  - check db for available enginners in ASC order lastClosedIssueDate. 
    - If there is one assign issue to him like:
    ```js
    // patch issue
    const patchObj = {
      status: IssueStatusEnum.ASSIGNED,
      supportAgentId: 23,
      // (updatedAt will be set by sequelize)
    }
    ```

### endpoint to mark issue complete PATCH: `api/v1/issues/:issue_id/complete`
  - mark issue as complete and update supportAgent
  ```js
  const issuePatchObj = {
    status: IssueStatusEnum.COMPLETE,
    supportAgentId: null,
    // (updatedAt will be set by sequelize)
  }
  const agentPatchObj = {
    lastClosedIssueDate: new Date()
  }
  ```
  - poll 'issueQue' for new issues
    - if no new issue, close
    - if there is open issue assign it to SupportAgent

