# Technologies
- node 12.*
- express with some async wrapper around router or koa 2.*
- AWS SQS for que service
- jest for testing
- TS
- postgres with sequelize 
