import { IssueController } from "../../../src/controllers";
import { issueService, supportAgentService } from "../../../src/services";
import * as uuid from 'uuid';
import {
  issueServiceStub,
  issueConsumerStub,
  issueProviderStub,
  supportAgentServiceStub,
  supportAgentConsumerStub,
  supportAgentProviderStub,
  issueMock
} from "../../stubs";
import { IssueStatusEnum, TestEntitiesPrefixesEnum } from "../../../src/constants";
import { DatabaseTableNames, db } from "../../../src/database";
import { QueryTypes } from "sequelize";

describe('[FunctionalTests] issue controller', () => {
  const issueController = new IssueController({
    issueService, // use real database
    issueConsumer: issueConsumerStub, // mock calls to SQS
    issueProvider: issueProviderStub,
    supportAgentService,
    supportAgentConsumer: supportAgentConsumerStub,
    supportAgentProvider: supportAgentProviderStub
  })
  describe('POST issue', () => {
    const mockDescription = `${uuid.v1()}_UNIQUE_TEST_DESCRIPTION`
    let user: unknown;
    beforeAll(async (done) => {
      // get test user
      [user] = await db.query(`SELECT * FROM "${DatabaseTableNames.user}"
       WHERE name LIKE '%${TestEntitiesPrefixesEnum.user}'`, { type: QueryTypes.SELECT });
      done();
    })
    afterAll((done) => {

    })
    it('correctly process new issue', async (done) => {
      // make controller request
      const response = await issueController.postIssue({ body: { description: mockDescription }, user } as any, {} as any, () => { });

      // check in response
      expect(response.data.issue.description).toBe(mockDescription);
      // issue should be assigned
      expect(response.data.issue.status).toBe(IssueStatusEnum.ASSIGNED.toString());

      // check if issue is in database
      const issue = await issueService.getOneByParams({ description: mockDescription });

      // expect issue to exist
      expect(issue).toBeTruthy();

      if (issue) {
        await issueService.deleteIssuesByParams({ id: issue.id })
      }

      done();
    })
  })
})

describe('[UnitTests] issue controller', () => {
  const issueController = new IssueController({
    // use only mock services
    issueService: issueServiceStub,
    issueConsumer: issueConsumerStub,
    issueProvider: issueProviderStub,
    supportAgentService: supportAgentServiceStub,
    supportAgentConsumer: supportAgentConsumerStub,
    supportAgentProvider: supportAgentProviderStub
  })
  describe('POST issue', () => {
    it('assigns new issue', async (done) => {
      const mockDescription = `${uuid.v1()}_UNIQUE_TEST_DESCRIPTION`;
      const response = await issueController.postIssue({ body: { description: mockDescription }, user: { id: 1 } } as any, {} as any, () => { });

      // issue should be assigned
      expect(response.data.issue.status).toBe(IssueStatusEnum.ASSIGNED);

      done();
    })
  })
})