import { Transaction } from "sequelize";
import { IssueStatusEnum } from "../../../../src/constants";
import { IssueDBModel } from "../../../../src/database"
import { IIssueService } from "../../../../src/services"
import { supportAgentStub } from "../../services";

export const issueMock: IssueDBModel = {
  status: IssueStatusEnum.UNASSIGNED,
  id: 42,
  description: 'interface FindOptions missing searchPath #12936',
  userId: 8,
  updatedAt: new Date(),
  createdAt: new Date(),
  supportAgentId: null
} as unknown as IssueDBModel;

class IssueServiceStub implements IIssueService {
  getOneByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel> {
    return Promise.resolve(issueMock as IssueDBModel);
  }
  createIssue(issue: { description: string, userId: number } & Partial<IssueDBModel>, transaction?: Transaction): Promise<IssueDBModel> {
    if (issue.status) {
      return Promise.resolve({ ...issueMock, status: issue.status } as IssueDBModel);
    }
    return Promise.resolve(issueMock as IssueDBModel);
  }
  deleteIssuesByParams(params: Partial<IssueDBModel>, transaction?: Transaction): Promise<number> {
    return Promise.resolve(1);
  }
  assignIssue(agentId: number, issueId: number, transaction?: Transaction): Promise<IssueDBModel> {
    return Promise.resolve({ ...issueMock, status: IssueStatusEnum.ASSIGNED, supportAgentId: supportAgentStub.id } as IssueDBModel);
  }
  markResolved(issueId: number, transaction?: Transaction): Promise<IssueDBModel> {
    return Promise.resolve({ ...issueMock, status: IssueStatusEnum.COMPLETE } as IssueDBModel);
  }
}

export const issueServiceStub = new IssueServiceStub();