import { Transaction } from "sequelize/types";
import { SupportAgentDBModel } from "../../../../src/database";
import { ISupportAgentService } from "../../../../src/services";

export const supportAgentStub = {
  id: 1,
  userId: 2,
  lastClosedIssueDate: null,
  createdAt: new Date(),
  updatedAt: null
} as unknown as SupportAgentDBModel;

class SupportAgentServiceStub implements ISupportAgentService {
  getOneByParams(params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel> {
    return Promise.resolve(supportAgentStub);
  }
  updateOneById(agentId: number, params: Partial<SupportAgentDBModel>, transaction?: Transaction): Promise<SupportAgentDBModel> {
    return Promise.resolve(supportAgentStub);
  }
}

export const supportAgentServiceStub = new SupportAgentServiceStub();