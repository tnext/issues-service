import { ISendMessageResult, ISupportAgentProvider } from "../../../../src/queues";
import { messsageIdMock } from "../issue/issue.provider.stub";

export class SupportAgentProviderStub implements ISupportAgentProvider {
  async addFreeAgent(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult> {
    return Promise.resolve({
      messageId: messsageIdMock
    });
  }
}

export const supportAgentProviderStub = new SupportAgentProviderStub();