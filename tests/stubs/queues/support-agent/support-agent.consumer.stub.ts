import { ISupportAgentConsumer, IMessageReceived } from "../../../../src/queues";
import { supportAgentStub } from "../../services";

export const agentMessageStub = {
  body: supportAgentStub,
  timestamp: '1609656007625',
  attributes: undefined,
  receiptHandle: 'AQEBk+u0hIsYkFQ0OCU9I3Fjg1Rp'
};

export class SupportAgentConsumerStub implements ISupportAgentConsumer {
  async getAgents(numberOfMessages = 10, attributes?: string[]): Promise<IMessageReceived[]> {
    return Promise.resolve([agentMessageStub]);
  }

  async getOneAgent(attributes?: string[]): Promise<IMessageReceived | null> {
    return Promise.resolve(agentMessageStub);
  }

  async deleteFreeAgent(receiptHandle: string): Promise<void> {
    return Promise.resolve();
  }
}

export const supportAgentConsumerStub = new SupportAgentConsumerStub();