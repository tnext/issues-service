import { IssueStatusEnum } from "../../../../src/constants";
import { IIssueConsumer, IMessageReceived } from "../../../../src/queues";

export const messageIssueStub = {
  body: {
    status: IssueStatusEnum.UNASSIGNED,
    id: 37,
    description: 'test Description',
    userId: 8,
    updatedAt: new Date(),
    createdAt: new Date(),
    supportAgentId: null
  },
  timestamp: '1609656007625',
  attributes: undefined,
  receiptHandle: 'AQEBk+u0hIsYkFQ0OCU9I3Fjg1Rp'
};

export class IssueConsumerStub implements IIssueConsumer {
  async getMessages(numberOfMessages = 10, attributes?: string[]): Promise<IMessageReceived[]> {
    return Promise.resolve([messageIssueStub]);
  }

  async getOneMessage(attributes?: string[]): Promise<IMessageReceived | null> {
    return Promise.resolve(messageIssueStub);
  }

  async delete(receiptHandle: string): Promise<void> {
    return Promise.resolve();
  }
}

export const issueConsumerStub = new IssueConsumerStub();