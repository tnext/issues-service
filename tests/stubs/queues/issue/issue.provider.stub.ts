import { ISendMessageResult, IIssueProvider } from "../../../../src/queues";

export const messsageIdMock = '3740d656-c56b-499f-89f9-b934bf78252f';

export class IssueProviderStub implements IIssueProvider {
  async addMessage(body: string, attributes?: Map<string, string>): Promise<ISendMessageResult> {
    return Promise.resolve({
      messageId: messsageIdMock
    });
  }
}

export const issueProviderStub = new IssueProviderStub();